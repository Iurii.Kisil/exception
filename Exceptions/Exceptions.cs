using System;
using System.Runtime.Serialization;

namespace Exceptions
{
    [Serializable]
    public class MatrixException : Exception
    {
        public MatrixException()
        {
        }

        public MatrixException(string message) : base(message)
        {
        }
        
        public MatrixException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }

        // Without this constructor, deserialization will fail
        protected MatrixException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }
    }
    
    public class Matrix
    {
        private enum Dimensions
        {
            Rows = 0,
            Columns = 1
        }

        public double[,] Array { get; private set; }

        public int Rows
        {
            get => Array.GetLength((int) Dimensions.Rows);
        }
        
        public int Columns
        {
            get => Array.GetLength((int) Dimensions.Columns);
        }
        
        public Matrix(int rows, int columns)
        {
            if (rows <= 0) throw new ArgumentOutOfRangeException(nameof(rows), "Rows must be above zero.");
            if (columns <= 0) throw new ArgumentOutOfRangeException(nameof(columns), "Columns must be above zero.");
            Array = new double[rows, columns];
        }
        
        public Matrix(double[,] array)
        {
            Array = array ?? throw new ArgumentNullException(nameof(array));
        }
        
        public double this[int row, int column]
        {
            get
            {
                try
                {
                    return Array[row, column];
                }
                catch
                {
                    throw new ArgumentException("Index is incorrect.");
                }
            } 
            set
            {
                try
                {
                    Array[row, column] = value;
                }
                catch
                {
                    throw new ArgumentException("Index is incorrect.");
                }
            } 
        }
        
        public Matrix Add(Matrix matrix)
        {
            if (matrix == null) throw new ArgumentNullException(nameof(matrix));
            
            if (matrix.Rows != Rows || matrix.Columns != Columns)
            {
                throw new MatrixException("Matrix dimensions are different.");
            }

            var result = new Matrix(matrix.Rows, matrix.Columns);
            for (var i = 0; i < matrix.Rows; i++)
            {
                for (var j = 0; j < matrix.Columns; j++)
                {
                    result[i, j] = matrix[i, j] + this[i, j];
                }
            }

            return result;
        }

        public Matrix Subtract(Matrix matrix)
        {
            if (matrix == null) throw new ArgumentNullException(nameof(matrix));

            if (matrix.Rows != this.Rows || matrix.Columns != this.Columns)
            {
                throw new MatrixException("Matrix dimensions are different.");
            }

            var result = new Matrix(matrix.Rows, matrix.Columns);
            for (var i = 0; i < matrix.Rows; i++)
            {
                for (var j = 0; j < matrix.Columns; j++)
                {
                    result[i, j] = this[i, j] - matrix[i, j];
                }
            }

            return result;
        }

        public Matrix Multiply(Matrix matrix)
        {
            //if (matrix == null) throw new ArgumentNullException(nameof(matrix));
            
            if (Columns != matrix.Rows)
            {
                throw new MatrixException("Matrix dimensions are different.");
            }
            
            var resultArray = new double[Rows, matrix.Columns];
            
            for (var i = 0; i < resultArray.GetLength((int) Dimensions.Rows); i++)
            {
                for (var j = 0; j < resultArray.GetLength((int) Dimensions.Columns); j++)
                {
                    resultArray[i, j] = 0;

                    for (var k = 0; k < Array.GetLength((int) Dimensions.Columns); k++)
                    {
                        resultArray[i, j] = resultArray[i, j] + Array[i, k] * matrix.Array[k, j];
                    }
                }
            }

            return new Matrix(resultArray);
        }
    }
}
